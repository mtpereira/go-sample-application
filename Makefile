TARGETS = build up provision deploy test

.PHONY: $(TARGETS) all clean

OS = linux
ARCH = amd64
BUILD = GOOS=$(OS) GOARCH=$(ARCH) go build
BINARY = go-sample-application

all: $(TARGETS)

build: main.go
	$(BUILD) -o ansible/files/$(BINARY) $<

up:
	vagrant up --provision-with shell

provision:
	ansible-playbook -i ansible/inventory ansible/all.yml -vv

deploy:
	ansible-playbook -i ansible/inventory ansible/deploy.yml -vv

test:
	vagrant provision --provision-with serverspec

clean:
	rm -f ansible/files/$(BINARY) && vagrant destroy --force

