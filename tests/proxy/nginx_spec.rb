describe 'nginx' do

  describe package('nginx') do
    it { should be_installed }
  end

  describe file('/etc/nginx/sites-available/proxy') do
    it { should be_file }
    it { should exist }
    it { should be_owned_by 'root' }
    it { should be_mode '644' }
    it { should contain ('listen 80;') }
    it { should contain ('proxy_pass http://backend;') }
    it { should contain ('server 10.0.142.3') }
    it { should contain ('server 10.0.142.4') }
  end

  describe file('/etc/nginx/sites-enabled/default') do
    it { should_not exist }
  end

  describe file('/etc/nginx/sites-enabled/proxy') do
    it { should be_owned_by 'root' }
    it { should be_symlink }
    it { should be_linked_to('/etc/nginx/sites-available/proxy') }
  end

  describe command('curl -s localhost:80') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should match /Hi there, I'm served from app0[1-2]!/ }
  end

end

