describe 'common' do

  describe file('/etc/hostname') do
    it { should be_file }
    its(:content) { should match /^app\d{2}$/ }
  end

  describe file('/etc/timezone') do
    it { should be_file }
    its(:content) { should match /^Europe\/London$/ }
  end

  describe service('ntp') do
    it { should be_running.under('systemd') }
  end

  describe file('/etc/ntp.conf') do
    it { should be_file }
    its(:content) { should match /server 0.uk.pool.ntp.org/ }
    its(:content) { should match /server 1.uk.pool.ntp.org/ }
    its(:content) { should match /server 2.uk.pool.ntp.org/ }
    its(:content) { should match /server 3.uk.pool.ntp.org/ }
  end

  describe command('ntpdc -l') do
    its(:stdout) { should_not match /Server reports data not found/ }
  end

end

