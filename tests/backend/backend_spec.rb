describe 'backend' do

  describe user('backend') do
    it { should exist }
    it { should have_home_directory '/dev/null' }
    it { should have_login_shell '/bin/false' }
  end

  describe process('go-sample-application') do
    it { should be_running }
    its(:user) { should eq "backend" }
    its(:count) { should eq 1 }
  end

  describe file('/srv/backend/go-sample-application') do
    it { should be_owned_by('root') }
    it { should be_mode('755') }
  end

  describe service('backend') do
    it { should be_running.under('systemd') }
  end

  describe command('curl -s localhost:8484') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should match /Hi there, I'm served from app\d{2}!/ }
  end

end

