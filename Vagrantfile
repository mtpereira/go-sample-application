# -*- mode: ruby -*-
# vim: set ft=ruby ts=2 sw=2 tw=0 et :

boxes = YAML::load(File.read(__dir__ + '/boxes.yml'))

Vagrant.configure("2") do |config|
  unless Vagrant.has_plugin? "vagrant-serverspec"
        puts <<-EOF
Serverspec plugin is not installed! To fix that:
* vagrant plugin install vagrant-serverspec
Run the tests with:
* vagrant provision --with-provider serverspec
EOF
  end

  boxes.each do |box|
    config.vm.define box[:name] do |vms|
      vms.vm.box = box[:box]
      vms.vm.hostname = "%s" % box[:name]

      vms.vm.provider "virtualbox" do |v|
        v.memory = box[:ram]
        v.cpus = box[:cpus]
        v.customize ["modifyvm", :id, "--cpuexecutioncap", box[:cpu_cap]]
      end

      vms.vm.network :private_network, ip: box[:ip]

      # allow sudoers to impersonate users
      vms.vm.provision :shell do |shell|
        shell.inline = "sed -i -e 's/^%sudo.*$/%sudo ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers"
      end

      vms.vm.provision :ansible do |ansible|
        ansible.playbook = "ansible/%s.yml" % box[:role]
        ansible.sudo = true

        ansible.groups = {
          box[:name] => box[:name],
          box[:role] => box[:name]
        }

        ansible.verbose = ENV['ANSIBLE_VERBOSE'] ||= "vv"
        ansible.host_key_checking = false

        if ENV.has_key?('ANSIBLE_TAGS')
          ansible.tags = ENV["ANSIBLE_TAGS"].split(',')
        end
      end

      if Vagrant.has_plugin? "vagrant-serverspec"
        vms.vm.provision :serverspec do |spec|
          spec.pattern = __dir__ + "/tests/#{box[:role]}/*_spec.rb"
        end
      end

    end
  end
end
