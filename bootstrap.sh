#!/usr/bin/env bash

set -eu

function brew_installed {
	brew list -1 \
		| grep -q -E "^$1$"
}
function brew_cask_installed {
	brew cask list -1 \
		| grep -q -E "^$1$"
}

function brew_cask_tapped {
	brew tap \
		| grep -q -E "^$1$"
}

function vagrant_plugin_installed {
	vagrant plugin list \
		| grep -q -E "^$1 (.+)$"
}

function _stat {
  if [ $(which stat) == "/usr/bin/stat" ]; then
    stat -f "%Su" $@
  else
    stat -c "%U" $@
  fi
}

function _run {
  local cmd="${1}"
  local cmd_owner=$(_stat $(command -v "${cmd}"))
  shift

  if [ "$cmd_owner" = "$USER" ]; then
    echo "==> Running ${cmd} $@..."
    "${cmd}" $@
  else
    echo "==> Requesting password for sudo ${cmd} $@..."
    sudo "${cmd}" $@
  fi
}

_run brew update

brew_installed python || _run brew install python
brew_installed go || _run brew install go

_run pip install --upgrade pip
_run pip install ansible==2.1.1.0

brew_cask_tapped caskroom/cask \
	|| ( _run brew tap caskroom/cask && _run brew update )
brew_cask_installed virtualbox || _run brew cask install virtualbox
brew_cask_installed vagrant || _run brew cask install vagrant

vagrant_plugin_installed vagrant-serverspec \
	|| vagrant plugin install vagrant-serverspec

