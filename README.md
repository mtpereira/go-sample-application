# Go-sample-application

Deploy a sample Go application on 2 nodes, with 1 nginx reverse proxy performing round-robin loadbalancing.

The nodes are all VirtualBox machines provisioned with Vagrant. The provision, configuration and testing flow is guided by a Makefile.

## Considerations and some notes

* Provision with Vagrant, configuration with Ansible and testing with Serverspec.
* Rolling deployment with zero-downtime using Ansible.
* Designed with a KISS mindset, hence the almost non-existing customization on
  the playbooks and roles behaviour through variables.
* Approached the problem with an configuration management and infrastructure as
  code mindset.
* List of hosts defined on an Ansible static inventory, which can be replaced
  by a dynamic inventory on a cloud provider.
* Future work on AWS or other providers should prove to be easy enough, since
  we're only relying on inventory groups for discovery (which can be set
  through AWS tags on a dynamic inventory, for instance) and we've got a
  variable for defining the backends' network interface.

## Usage

**TL;DR:** `./bootstrap.sh && make all`

### Setup your local environment

Assuming you're on a recent macOS environment, run the following script:

* `./bootstrap.sh`

**Do read** the script before running it, to ensure that it won't break your particular setup.

### Build the application, provision, configure and test the infrastructure

* `make all`

### Run the Serverspec tests

* `make test`

### Build the application

* `make build`

### Teardown the infrastructure

* `make clean`

### Perform a rolling deploy

* `make deploy`

## TODO

- [X] add zero-downtime deployment
- [ ] deploy on changes to the application's source
- [ ] AWS provision

